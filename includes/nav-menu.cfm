<header class="topbar">
    <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="/index.cfm">
                <b>
                    <img src="assets/images/logo.png" alt="homepage" class="dark-logo" />
                </b>
            </a>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto mt-md-0 ">
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <!--- --->
            </ul>
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="userProfile.cfm?uid=1" ><img src="assets/images/users/1.jpg" alt="user" class="profile-pic m-r-5" />Markarn Doe</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li>
                    <a href="/" class="waves-effect"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i>Dashboard</a>
                </li>
                <li>
                    <a href="userList.cfm" class="waves-effect"><i class="fa fa-user m-r-10" aria-hidden="true"></i>Users</a>
                </li>
                <li>
                    <a href="attendance.cfm" class="waves-effect"><i class="fa fa-table m-r-10" aria-hidden="true"></i>Attendance</a>
                </li>
                <li>
                    <a href="department.cfm" class="waves-effect"><i class="fa fa-font m-r-10" aria-hidden="true"></i>Department</a>
                </li>
               <!---  <li>
                    <a href="map-google.html" class="waves-effect"><i class="fa fa-globe m-r-10" aria-hidden="true"></i>Google Map</a>
                </li>
                <li>
                    <a href="pages-blank.html" class="waves-effect"><i class="fa fa-columns m-r-10" aria-hidden="true"></i>Blank Page</a>
                </li>
                <li>
                    <a href="pages-error-404.html" class="waves-effect"><i class="fa fa-info-circle m-r-10" aria-hidden="true"></i>Error 404</a>
                </li> --->
            </ul>
        </nav>
    </div>
</aside>