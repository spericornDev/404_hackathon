<cfinclude template="/includes/header.cfm" >
<cfoutput>
	<body class="fix-header fix-sidebar card-no-border">
	    <div id="main-wrapper">
			<cfinclude template="/includes/nav-menu.cfm">
	        <div class="page-wrapper">
	        	<div class="container-fluid">
	        		<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Table</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Table</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Attendance</h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>##</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<cfloop from="1" to="10" index="item">
	                                            <tr>
	                                                <td>1</td>
	                                                <td>Deshmukh</td>
	                                                <td>Prohaska</td>
	                                                <td>@Genelia</td>
	                                                <td><a href=""><i class="fa fa-eye text-info"></i></a></td>
	                                            </tr>
	                                        </cfloop>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	        	</div>
	        	<cfinclude template="/includes/footer.cfm" >
	        </div>
	    </div>
	    <cfinclude template="/includes/bottom.cfm" >
	</body>
</cfoutput>