<cfinclude template="/includes/header.cfm" >
<cfoutput>
	<body class="fix-header fix-sidebar card-no-border">
	    <div id="main-wrapper">
			<cfinclude template="/includes/nav-menu.cfm">
	        <div class="page-wrapper">
	        	<div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col-md-6 col-8 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0">Profile</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Profile</li>
                            </ol>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="row">
                        <!-- Column -->
                        <div class="col-lg-4 col-xlg-3 col-md-5">
                            <div class="card">
                                <div class="card-block">
                                    <center class="m-t-30"> <img src="assets/images/users/5.jpg" class="img-circle userPic" width="150" />
										<div class="p-image">
											<i class="fa fa-camera upload-button" ></i>
											<input class="file-upload" type="file" accept="image/*"/>
										</div>
										<cfif structKeyExists(url, 'uid') and len(trim(url.uid)) and trim(url.uid)>
	                                        <h4 class="card-title m-t-10">Hanna Gover</h4>
	                                        <h6 class="card-subtitle">Manager</h6>
	                                        <div class="row text-center justify-content-md-center">
	                                            <div class="col-4"><i class="fa fa-id-card" aria-hidden="true"></i><font class="font-medium"> SPO61</font></div>
	                                        </div>
	                                    </cfif>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-8 col-xlg-9 col-md-7">
                            <div class="card">
                                <div class="card-block">
                                    <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12">EMP ID</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="SPxxx" class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">First Name</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="" class="form-control form-control-line" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Last Name</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="" class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email" class="col-md-12">Official Email</label>
                                            <div class="col-md-12">
                                                <input type="email" placeholder="xxxx@xx.com" class="form-control form-control-line" name="example-email" id="example-email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email" class="col-md-12">Personal Email</label>
                                            <div class="col-md-12">
                                                <input type="email" placeholder="xxxx@xx.com" class="form-control form-control-line" name="example-email" id="example-email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Contact</label>
                                            <div class="col-md-12">
                                                <input type="tel" placeholder="" class="form-control form-control-line">
                                            </div>

                                            <div class="form-group">
	                                            <label class="col-sm-12">Department</label>
	                                            <div class="col-sm-12">
	                                                <select class="form-control form-control-line">
	                                                    <option>C-Level</option>
	                                                    <option>Management</option>
	                                                    <option>Delivery</option>
	                                                    <option>Finance</option>
	                                                </select>
	                                            </div>
	                                        </div>

                                            <div class="form-group">
	                                            <label class="col-sm-12">Position</label>
	                                            <div class="col-sm-12">
	                                                <!--- <input class="form-control form-control-line" id="country_name" name="country_name" type="text" list="Position" />
	                                                <datalist id="position">
	                                                    <option value="">Manager</option>
	                                                    <option value="">Developer</option>
	                                                    <option value="">Tester</option>
	                                                    <option value="">Accountant</option>
	                                                    <option value="">System Admin</option>
	                                                 </datalist> --->
	                                                 <select class="form-control form-control-line">
	                                                    <option value="">--Select--</option>
	                                                    <option value="">Manager</option>
	                                                    <option value="">Developer</option>
	                                                    <option value="">Tester</option>
	                                                    <option value="">Accountant</option>
	                                                    <option value="">System Admin</option>
	                                                </select>
	                                            </div>
	                                        </div>

                                            <div class="form-group">
	                                            <label class="col-sm-12">User Role</label>
	                                            <div class="col-sm-12">
	                                                <!--- <input class="form-control form-control-line" id="country_name" name="country_name" type="text" list="user_roles" />
	                                                <datalist id="user_roles">
	                                                    <option value="">Super Admin</option>
	                                                    <option value="">Admin</option>
	                                                    <option value="">User</option>
	                                                 </datalist> --->
	                                                 <select class="form-control form-control-line">
	                                                 	 <option value="">--Select--</option>
	                                                 	 <option value="">Super Admin</option>
	                                                    <option value="">Admin</option>
	                                                    <option value="">User</option>
	                                                 </select>
	                                            </div>
                                    		</div>

                                    		<div class="form-group">
                                    			<div class="col-sm-12">
													<div class="form-check-inline">
														<label class="form-check-label">
															<input type="checkbox" class="form-check-input" value=""> Office 1(Main Office)
														</label>
													</div>
													<div class="form-check-inline">
														<label class="form-check-label">
															<input type="checkbox" class="form-check-input" value=""> Office 2
														</label>
													</div>
												</div>
												<!--- <div class="form-check-inline">
													<label class="form-check-label">
														<input type="checkbox" class="form-check-input" value="" disabled>Option 3
													</label>
												</div> --->
											</div>
	                                        <div class="form-group">
	                                            <div class="col-sm-12">
	                                                <button class="btn btn-success">Update Profile</button>
	                                            </div>
	                                        </div>
	                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                </div>
	        <cfinclude template="/includes/footer.cfm" >
	        </div>
	    </div>
	    <cfinclude template="/includes/bottom.cfm" >
	</body>
</cfoutput>