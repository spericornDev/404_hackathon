<cfinclude template="/includes/header.cfm" >
<cfoutput>
	<body class="fix-header fix-sidebar card-no-border">
	    <div id="main-wrapper">
			<cfinclude template="/includes/nav-menu.cfm">
	        <div class="page-wrapper">
	            <div class="container-fluid">
	                <div class="row page-titles">
	                    <div class="col-md-6 col-8 align-self-center">
	                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
	                        <ol class="breadcrumb">
	                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
	                            <li class="breadcrumb-item active">Dashboard</li>
	                        </ol>
	                    </div>
	                </div>
	                <!-- Row -->
	                <div class="row">
	                    <!-- Column -->
	                    <div class="col-sm-6">
	                        <div class="card">
	                            <div class="card-block">
	                                <h4 class="card-title">Present</h4>
	                                <div class="text-right">
	                                    <h2 class="font-light m-b-0"><i class="fa fa-users userIcon" aria-hidden="true"></i>80</h2>
	                                    <span class="text-muted">#DateFormat(Now(),"mm/dd/yyyy")# </span>
	                                </div>
	                                <span class="text-success">80%</span>
	                                <div class="progress">
	                                    <div class="progress-bar bg-success" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- Column -->
	                    <!-- Column -->
	                    <div class="col-sm-6">
	                        <div class="card">
	                            <div class="card-block">
	                                <h4 class="card-title">Absent</h4>
	                                <div class="text-right">
	                                    <h2 class="font-light m-b-0"><i class="fa fa-users userIcon" aria-hidden="true"></i>20</h2>
	                                    <span class="text-muted">#DateFormat(Now(),"mm/dd/yyyy")# </span>
	                                </div>
	                                <span class="text-info">30%</span>
	                                <div class="progress">
	                                    <div class="progress-bar bg-info" role="progressbar" style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- Column -->
	                </div>
	                <!-- Row -->
	                <!-- Row -->
	                <div class="row">
	                    <div class="col-sm-12">
	                        <div class="card">
	                            <div class="card-block">
	                                <h4 class="card-title">Projects of the Month</h4>
	                                <div class="table-responsive m-t-40">
	                                    <table class="table stylish-table">
	                                        <thead>
	                                            <tr>
	                                                <th colspan="2">Assigned</th>
	                                                <th>Department</th>
	                                                <th>Logged In</th>
	                                                <th>Active</th>
	                                                <th>Action</th>
	                                            </tr>
	                                        </thead>
	                                        <tbody>
	                                        	<cfloop from="1" to="10" index="item">
		                                            <tr>
		                                                <td style="width:50px;">
		                                                	<span class="round">S</span>
		                                                </td>
		                                                <td>
		                                                    <h6>Sunil Joshi</h6><small class="text-muted">Web Designer</small>
		                                                </td>
		                                                <td>C-Level</td>
		                                                <td>09:00 AM</td>
		                                                <td>
		                                                	<i class="fa fa-circle text-success"></i>
		                                                	<!--- <i class="fa fa-circle text-danger"></i> --->
		                                                </td>
		                                                <td><a href=""><i class="fa fa-eye text-info"></i></a></td>
		                                            </tr>
		                                        </cfloop>
	                                        </tbody>
	                                    </table>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <!-- Row -->
	                <!-- Row -->
	                <div class="row">
	                    <!-- Column -->
	                    <div class="col-lg-4">
	                        <div class="card">
	                            <div class="cardImage">
	                            	<img class="card-img-top img-responsive" src="assets/images/big/hackathon.png" alt="Card">
	                            </div>
	                            <div class="card-block cardEvent">
	                                <ul class="list-inline font-14">
	                                    <li class="p-l-0">31 Aus 2019</li>
	                                </ul>
	                                <h3 class="font-normal">Hackathon</h3>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- Column -->
	                    <!-- Column -->
	                    <div class="col-lg-4">
	                        <div class="card">
	                            <div class="cardImage">
	                            	<img class="card-img-top img-responsive" src="assets/images/big/Onam2.jpeg" alt="Card">
	                            </div>
	                            <div class="card-block cardEvent">
	                                <ul class="list-inline font-14">
	                                    <li class="p-l-0">06 Sep 2019</li>
	                                </ul>
	                                <h3 class="font-normal">Onam Celebration</h3>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- Column -->
	                    <!-- Column -->
	                    <div class="col-lg-4">
	                        <div class="card">
	                            <div class="cardImage">
	                            	<img class="card-img-top img-responsive" src="assets/images/big/xmas.jpeg" alt="Card">
	                            </div>
	                            <div class="card-block cardEvent">
	                                <ul class="list-inline font-14">
	                                    <li class="p-l-0">25 Dec 2019</li>
	                                </ul>
	                                <h3 class="font-normal">X'mas</h3>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- Column -->
	                </div>
	                <!-- Row -->
	            </div>
	            <cfinclude template="/includes/footer.cfm" >
	        </div>
	    </div>
	    <cfinclude template="/includes/bottom.cfm" >
	</body>
</cfoutput>