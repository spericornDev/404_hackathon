<cfinclude template="/includes/header.cfm" >
<cfoutput>
	<body class="fix-header fix-sidebar card-no-border">
	    <div id="main-wrapper">
			<cfinclude template="/includes/nav-menu.cfm">
	        <div class="page-wrapper">
	        	<div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col-md-6 col-8 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0">Add Department</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Add Department</li>
                            </ol>
                        </div>
                    </div>
                    <!-- Row -->
                    <div class="row  align-self-center">
                        <!-- Column -->
                        <div class="col-lg-8 col-xlg-9 col-md-7">
                            <div class="card">
                                <div class="card-block">
                                    <form class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12">Department</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="SPxxx" class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email" class="col-md-12">Department Email</label>
                                            <div class="col-md-12">
                                                <input type="email" placeholder="email@gmail.com" class="form-control form-control-line" name="example-email" id="example-email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email" class="col-md-12">Department Head</label>
                                            <div class="col-md-12">
                                               <select class="form-control form-control-line">
                                                    <option>--Select--</option>
                                                    <option>employee Name</option>
                                                    <option>employee Name</option>
                                                    <option>employee Name</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
	                                        <div class="form-group">
	                                            <div class="col-sm-12">
	                                                <button class="btn btn-success">Update</button>
	                                            </div>
	                                        </div>
	                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                </div>
	        <cfinclude template="/includes/footer.cfm" >
	        </div>
	    </div>
	    <cfinclude template="/includes/bottom.cfm" >
	</body>
</cfoutput>